#!/bin/sh

echo "[INFO] Compiling $1"
javac -cp ".;*" $1.java

echo "[INFO] Running $1"
java -XX:+UseSerialGC -XX:TieredStopAtLevel=1 -XX:NewRatio=5 -Xms8M -Xmx512M -Xss64M -DONLINE_JUDGE=true Solution