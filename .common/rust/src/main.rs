#![allow(unused_imports)]

// region ‹use›
use self::aliases::*;
use self::data_structures::*;
use self::input::*;
use self::math::*;
use self::output::*;
use std::cmp::{max, min};
// endregion ‹use›

fn solve(s: &mut Scanner) {
    todo!()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn example_1() {
        assert_eq!(1, 2);
    }
}

// region runner
const SINGLE_TEST: bool = false;
fn main() {
    let mut s = Scanner::new();

    let n: usize = if SINGLE_TEST { 1 } else { s.next() };
    for _ in 0..n {
        solve(&mut s)
    }
}
// endregion runner

#[allow(dead_code)]
mod aliases {
    use std::collections::{BTreeMap, BTreeSet, VecDeque};

    pub type V<T> = Vec<T>;
    pub type M<K, V> = BTreeMap<K, V>;
    pub type S<T> = BTreeSet<T>;
    pub type Q<T> = VecDeque<T>;
}

mod data_structures {
    use std::cmp::{Ord, Reverse};
    use std::collections::BinaryHeap;

    #[derive(Debug)]
    pub struct MinHeap<T> {
        heap: BinaryHeap<Reverse<T>>,
    }

    impl<T: Ord> MinHeap<T> {
        pub fn new() -> MinHeap<T> {
            MinHeap {
                heap: BinaryHeap::new(),
            }
        }

        pub fn push(&mut self, item: T) {
            self.heap.push(Reverse(item))
        }

        pub fn pop(&mut self) -> Option<T> {
            self.heap.pop().map(|Reverse(x)| x)
        }
    }

    impl<T: Ord> Default for MinHeap<T> {
        fn default() -> Self {
            Self::new()
        }
    }
}

#[allow(dead_code)]
mod input {
    use std::collections::VecDeque;
    use std::io;
    use std::str::FromStr;

    pub struct Scanner {
        buffer: VecDeque<String>,
    }

    impl Scanner {
        pub fn new() -> Scanner {
            Scanner {
                buffer: VecDeque::new(),
            }
        }

        pub fn next<T: FromStr>(&mut self) -> T {
            if self.buffer.is_empty() {
                let mut input = String::new();

                io::stdin().read_line(&mut input).ok();

                for word in input.split_whitespace() {
                    self.buffer.push_back(word.to_string())
                }
            }

            let front = self.buffer.pop_front().unwrap();
            front.parse::<T>().ok().unwrap()
        }

        pub fn collect<C: FromIterator<T>, T: FromStr>(&mut self, n: usize) -> C {
            (0..n).map(|_| self.next()).collect()
        }
    }
}

#[allow(dead_code)]
mod math {
    #[derive(Copy, Clone, Default)]
    pub struct Z(i64);

    impl Z {
        const MOD: i64 = 1_000_000_007;

        pub fn new(x: i64) -> Z {
            Z(x.rem_euclid(Z::MOD))
        }

        pub fn pow(self, mut exp: u32) -> Z {
            let mut ans = Z::new(1);
            let mut base = self;
            while exp > 0 {
                if exp % 2 == 1 {
                    ans *= base;
                }
                base *= base;
                exp >>= 1;
            }
            ans
        }

        pub fn inv(self) -> Z {
            assert_ne!(self.0, 0);
            self.pow((Z::MOD - 2) as u32)
        }
    }

    impl std::ops::Neg for Z {
        type Output = Z;

        fn neg(self) -> Z {
            Z::new(-self.0)
        }
    }

    impl std::ops::Add<Z> for Z {
        type Output = Z;

        fn add(self, rhs: Z) -> Z {
            Z::new(self.0 + rhs.0)
        }
    }

    impl std::ops::Sub<Z> for Z {
        type Output = Z;

        fn sub(self, rhs: Z) -> Z {
            Z::new(self.0 - rhs.0)
        }
    }

    impl std::ops::Mul<Z> for Z {
        type Output = Z;

        fn mul(self, rhs: Z) -> Z {
            Z::new(self.0 * rhs.0)
        }
    }

    impl std::ops::Div<Z> for Z {
        type Output = Z;

        fn div(self, rhs: Z) -> Z {
            #![allow(clippy::suspicious_arithmetic_impl)]
            self * rhs.inv()
        }
    }

    impl std::ops::AddAssign<Z> for Z {
        fn add_assign(&mut self, rhs: Z) {
            *self = *self + rhs;
        }
    }

    impl std::ops::SubAssign<Z> for Z {
        fn sub_assign(&mut self, rhs: Z) {
            *self = *self - rhs;
        }
    }

    impl std::ops::MulAssign<Z> for Z {
        fn mul_assign(&mut self, rhs: Z) {
            *self = *self * rhs;
        }
    }

    impl std::ops::DivAssign<Z> for Z {
        fn div_assign(&mut self, rhs: Z) {
            *self = *self / rhs;
        }
    }

    impl std::fmt::Display for Z {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.0.fmt(f)
        }
    }

    impl std::fmt::Debug for Z {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.0.fmt(f)
        }
    }

    impl std::str::FromStr for Z {
        type Err = std::num::ParseIntError;

        fn from_str(s: &str) -> Result<Self, Self::Err> {
            Ok(Z::new(s.parse()?))
        }
    }

    /// A trait implementing the unsigned bit shifts.
    pub trait UnsignedShift {
        fn unsigned_shl(self, n: u32) -> Self;
        fn unsigned_shr(self, n: u32) -> Self;
    }

    /// A trait implementing the integer square root.
    pub trait ISqrt {
        fn isqrt(&self) -> Self
        where
            Self: Sized,
        {
            self.isqrt_checked()
                .expect("cannot calculate square root of negative number")
        }

        fn isqrt_checked(&self) -> Option<Self>
        where
            Self: Sized;
    }

    macro_rules! math_traits_impl {
        ($T:ty, $U: ty) => {
            impl UnsignedShift for $T {
                #[inline]
                fn unsigned_shl(self, n: u32) -> Self {
                    ((self as $U) << n) as $T
                }

                #[inline]
                fn unsigned_shr(self, n: u32) -> Self {
                    ((self as $U) >> n) as $T
                }
            }

            impl ISqrt for $T {
                #[inline]
                fn isqrt_checked(&self) -> Option<Self> {
                    use core::cmp::Ordering;
                    match self.cmp(&<$T>::default()) {
                        // Hopefully this will be stripped for unsigned numbers (impossible condition)
                        Ordering::Less => return None,
                        Ordering::Equal => return Some(<$T>::default()),
                        _ => {}
                    }

                    // Compute bit, the largest power of 4 <= n
                    let max_shift: u32 = <$T>::default().leading_zeros() - 1;
                    let shift: u32 = (max_shift - self.leading_zeros()) & !1;
                    let mut bit = <$T>::try_from(1).unwrap().unsigned_shl(shift);

                    // Algorithm based on the implementation in:
                    // https://en.wikipedia.org/wiki/Methods_of_computing_square_roots#Binary_numeral_system_(base_2)
                    // Note that result/bit are logically unsigned (even if T is signed).
                    let mut n = *self;
                    let mut result = <$T>::default();
                    while bit != <$T>::default() {
                        if n >= (result + bit) {
                            n -= result + bit;
                            result = result.unsigned_shr(1) + bit;
                        } else {
                            result = result.unsigned_shr(1);
                        }
                        bit = bit.unsigned_shr(2);
                    }
                    Some(result)
                }
            }
        };
    }

    math_traits_impl!(i8, u8);
    math_traits_impl!(u8, u8);
    math_traits_impl!(i16, u16);
    math_traits_impl!(u16, u16);
    math_traits_impl!(i32, u32);
    math_traits_impl!(u32, u32);
    math_traits_impl!(i64, u64);
    math_traits_impl!(u64, u64);
    math_traits_impl!(i128, u128);
    math_traits_impl!(u128, u128);
    math_traits_impl!(isize, usize);
    math_traits_impl!(usize, usize);
}

#[allow(dead_code)]
mod output {
    pub fn yes() {
        println!("YES");
    }

    pub fn no() {
        println!("NO");
    }

    pub fn yesno(ans: bool) {
        println!("{}", if ans { "YES" } else { "NO" });
    }

    pub fn print_vec<T: Display>(v: &[T]) {
        print!("{}", v[0]);

        for i in 1..v.len() {
            print!(" {}", v[i]);
        }
        println!();
    }
}
