#include <cstdint>
#include <iostream>

namespace {

void solve() {
  int64_t n, m, a;
  std::cin >> n >> m >> a;

  int64_t n_needed = n / a + (n % a > 0);
  int64_t m_needed = m / a + (m % a > 0);

  std::cout << (n_needed * m_needed) << "\n";
}

}  // namespace

int main(void) {
  solve();
  return 0;
}
