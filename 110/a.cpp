#include <cstdint>
#include <iostream>

namespace helpers {

using namespace std;

long pow(long base, long exp) {
  if (exp == 0) return 1;
  long half = pow(base, exp / 2);
  if (exp % 2 == 0) return half * half;
  return half * half * base;
}

template <typename T>
inline void answer(const T &ans) {
  cout << ans << "\n";
}

inline void yes() { cout << "YES\n"; }
inline void no() { cout << "NO\n"; }

}  // namespace helpers

namespace {

using namespace std;
using namespace helpers;

int count_lucky(uint64_t n) {
  int lucky = 0;

  for (; n > 0; n /= 10) {
    if (n % 10 == 7 || n % 10 == 4) {
      ++lucky;
    }
  }

  return lucky;
}

bool nearly_lucky(uint64_t n) {
  auto count = count_lucky(n);

  for (auto c = count_lucky(n); c > 0; c /= 10) {
    if (c % 10 != 4 && c % 10 != 7) {
      return false;
    }
  }

  return count != 0;
}

void solve() {
  uint64_t n;
  cin >> n;

  if (nearly_lucky(n)) {
    yes();
  } else {
    no();
  }
}

}  // namespace

// for single test case, comment out for ‹N› test cases
#define SINGLE

#ifdef TEST

#include "../.common/cpp/catch_amalgamated.hpp"

TEST_CASE("examples") {
  CHECK(!nearly_lucky(40047));
  CHECK(nearly_lucky(7747774));
  CHECK(!nearly_lucky(1000000000000000000));
}

TEST_CASE("regression (probably platform-dependant size of ‹unsigned long›)") {
  CHECK(nearly_lucky(4744000695826));
}

#else

int main(void) {

#ifdef SINGLE

  solve();

#else

  // for multiple test cases
  int N;
  std::cin >> N >> std::ws;

  for (auto i = 0; i < N; ++i) {
    solve();
  }

#endif

  return 0;
}

#endif
