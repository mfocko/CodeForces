//import readInt

fun removeTrailingZeros(n: Int): Int =
    if (n % 10 == 0)
        removeTrailingZeros(n / 10)
    else
        n

fun f(n: Int): Int = removeTrailingZeros(n + 1)

fun findReachable(n: Int): Int {
    val reachable = mutableSetOf<Int>()
    var m = n

    while (reachable.add(m)) m = f(m)

    return reachable.size
}

fun main() {
    val n = readLine()!!.toInt()
    println(findReachable(n))
}
