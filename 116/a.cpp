#include <algorithm>
#include <cassert>
#include <cctype>
#include <cstdint>
#include <functional>
#include <iostream>
#include <optional>
#include <sstream>
#include <string>
#include <vector>

namespace helpers {

using namespace std;

long pow(long base, long exp) {
  if (exp == 0) return 1;
  long half = pow(base, exp / 2);
  if (exp % 2 == 0) return half * half;
  return half * half * base;
}

#define LOOP(n) for (auto i = 0; i < n; ++i)

template <typename T>
inline void answer(const T& ans) {
  cout << ans << "\n";
}

inline void yes() { cout << "YES\n"; }
inline void no() { cout << "NO\n"; }

}  // namespace helpers

namespace {

using namespace std;
using namespace helpers;

struct stop {
  int exit;
  int entry;
};

int min_capacity(const std::vector<stop>& stops) {
  int current = 0;
  int min_cap = 0;

  for (const auto& s : stops) {
    current += (s.entry - s.exit);
    min_cap = max(min_cap, current);
  }

  return min_cap;
}

void solve() {
  int N;
  cin >> N;

  std::vector<stop> stops;
  LOOP(N) {
    int exit, entry;
    cin >> exit >> entry;

    stops.push_back({exit, entry});
  }

  answer(min_capacity(stops));
}

}  // namespace

// for single test case, comment out for ‹N› test cases
#define SINGLE

#ifdef TEST

#include "../.common/cpp/catch_amalgamated.hpp"

TEST_CASE("examples") {
  CHECK(min_capacity(std::vector<stop>{{0, 3}, {2, 5}, {4, 2}, {4, 0}}) == 6);
}

#else

int main(void) {

#ifdef SINGLE

  solve();

#else

  // for multiple test cases
  int N;
  std::cin >> N >> std::ws;

  for (auto i = 0; i < N; ++i) {
    solve();
  }

#endif

  return 0;
}

#endif
