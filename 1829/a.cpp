#include <algorithm>
#include <cassert>
#include <cctype>
#include <cstdint>
#include <functional>
#include <iostream>
#include <optional>
#include <sstream>
#include <string>
#include <vector>

namespace helpers {

using namespace std;

long pow(long base, long exp) {
  if (exp == 0) return 1;
  long half = pow(base, exp / 2);
  if (exp % 2 == 0) return half * half;
  return half * half * base;
}

template <typename T>
inline void answer(const T& ans) {
  cout << ans << "\n";
}

inline void yes() { cout << "YES\n"; }
inline void no() { cout << "NO\n"; }

}  // namespace helpers

namespace {

using namespace std;
using namespace helpers;

static const string S("codeforces");

int differs(const string& s) {
  int counter = 0;
  for (auto i = 0u; i < S.size(); ++i) {
    if (s[i] != S[i]) {
      ++counter;
    }
  }

  return counter;
}

void solve() {
  string s;
  cin >> s;
  answer(differs(s));
}

}  // namespace

// for single test case, comment out for ‹N› test cases
// #define SINGLE

#ifdef TEST

#include "../.common/cpp/catch_amalgamated.hpp"

TEST_CASE("examples") {
  CHECK(differs("coolforsez") == 4);
  CHECK(differs("cadafurcie") == 5);
  CHECK(differs("codeforces") == 0);
  CHECK(differs("paiuforces") == 4);
  CHECK(differs("forcescode") == 9);
}

#else

int main(void) {

#ifdef SINGLE

  solve();

#else

  // for multiple test cases
  int N;
  std::cin >> N >> std::ws;

  for (auto i = 0; i < N; ++i) {
    solve();
  }

#endif

  return 0;
}

#endif
