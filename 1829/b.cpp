#include <algorithm>
#include <cassert>
#include <cctype>
#include <cstdint>
#include <functional>
#include <iostream>
#include <optional>
#include <sstream>
#include <string>
#include <vector>

namespace helpers {

using namespace std;

long pow(long base, long exp) {
  if (exp == 0) return 1;
  long half = pow(base, exp / 2);
  if (exp % 2 == 0) return half * half;
  return half * half * base;
}

template <typename T>
inline void answer(const T& ans) {
  cout << ans << "\n";
}

inline void yes() { cout << "YES\n"; }
inline void no() { cout << "NO\n"; }

}  // namespace helpers

namespace {

using namespace std;
using namespace helpers;

int longest(const vector<int>& arr) {
  int found = 0;

  int current = 0;
  for (auto i = 0u; i < arr.size(); ++i) {
    if (arr[i] == 0) {
      ++current;
      found = max(found, current);
    } else {
      current = 0;
    }
  }

  return found;
}

void solve() {
  int N;
  cin >> N;

  vector<int> arr(N);
  for (auto i = 0; i < N; ++i) {
    cin >> arr[i];
  }

  answer(longest(arr));
}

}  // namespace

// for single test case, comment out for ‹N› test cases
// #define SINGLE

#ifdef TEST

#include "../.common/cpp/catch_amalgamated.hpp"

TEST_CASE("examples") {
  CHECK(longest(vector<int>{1, 0, 0, 1, 0}) == 2);
  CHECK(longest(vector<int>{0, 1, 1, 1}) == 1);
  CHECK(longest(vector<int>{0}) == 1);
  CHECK(longest(vector<int>{1, 1, 1}) == 0);
  CHECK(longest(vector<int>{1, 0, 0, 0, 1, 0, 0, 0, 1}) == 3);
}

#else

int main(void) {

#ifdef SINGLE

  solve();

#else

  // for multiple test cases
  int N;
  std::cin >> N >> std::ws;

  for (auto i = 0; i < N; ++i) {
    solve();
  }

#endif

  return 0;
}

#endif
