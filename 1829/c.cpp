#include <algorithm>
#include <cassert>
#include <cctype>
#include <cstdint>
#include <functional>
#include <iostream>
#include <optional>
#include <sstream>
#include <string>
#include <vector>

namespace helpers {

using namespace std;

long pow(long base, long exp) {
  if (exp == 0) return 1;
  long half = pow(base, exp / 2);
  if (exp % 2 == 0) return half * half;
  return half * half * base;
}

template <typename T>
inline void answer(const T &ans) {
  cout << ans << "\n";
}

inline void yes() { cout << "YES\n"; }
inline void no() { cout << "NO\n"; }

#define LOOP(n) for (auto i = 0; i < n; ++i)

}  // namespace helpers

namespace {
using namespace std;
using namespace helpers;

struct book {
  int t;
  string skills;
};

int find_minimum(const vector<book> &books) {
  const int UNDEFINED = 400001;

  int min_a = UNDEFINED;
  int min_b = UNDEFINED;
  int min_both = UNDEFINED;

  for (const book &b : books) {
    if (b.skills == "01") {
      min_b = min(min_b, b.t);
    } else if (b.skills == "10") {
      min_a = min(min_a, b.t);
    } else if (b.skills == "11") {
      min_both = min(min_both, b.t);
    }
  }

  if (min_a != UNDEFINED && min_b != UNDEFINED && min_a + min_b < min_both) {
    return min_a + min_b;
  } else if (min_both != UNDEFINED) {
    return min_both;
  }

  return -1;
}

void solve() {
  int N;
  cin >> N;

  vector<book> bs(N);
  LOOP(N) {
    int t;
    string skills;
    cin >> t >> skills;
    bs[i] = {t, skills};
  }

  answer(find_minimum(bs));
}

}  // namespace

// for single test case, comment out for ‹N› test cases
// #define SINGLE

#ifdef TEST

#include "../.common/cpp/catch_amalgamated.hpp"

TEST_CASE("examples") {
  CHECK(find_minimum(
            vector<book>{{2, "00"}, {3, "10"}, {4, "01"}, {4, "00"}}) == 7);
  CHECK(find_minimum(vector<book>{
            {3, "01"}, {3, "01"}, {5, "01"}, {2, "10"}, {9, "10"}}) == 5);
  CHECK(find_minimum(vector<book>{{5, "11"}}) == 5);
  CHECK(find_minimum(vector<book>{{9, "11"}, {8, "01"}, {7, "10"}}) == 9);
  CHECK(
      find_minimum(vector<book>{
          {4, "01"}, {6, "01"}, {7, "01"}, {8, "00"}, {9, "01"}, {1, "00"}}) ==
      -1);
  CHECK(find_minimum(vector<book>{{{200000, "01"}, {200000, "10"}}}) == 400000);
}

#else

int main(void) {

#ifdef SINGLE

  solve();

#else

  // for multiple test cases
  int N;
  std::cin >> N >> std::ws;

  for (auto i = 0; i < N; ++i) {
    solve();
  }

#endif

  return 0;
}

#endif
