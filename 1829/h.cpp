#include <algorithm>
#include <bit>
#include <cassert>
#include <cctype>
#include <cstdint>
#include <functional>
#include <iostream>
#include <map>
#include <optional>
#include <queue>
#include <set>
#include <sstream>
#include <string>
#include <vector>

template <typename T, typename U>
std::ostream &operator<<(std::ostream &s, std::pair<T, U> const &p) {
  return s << p.first << " " << p.second;
}

namespace helpers {

using namespace std;

namespace math {

static constexpr int MODULO = 1000000007;

long pow(long base, long exp) {
  if (exp == 0) return 1;
  long half = pow(base, exp / 2);
  if (exp % 2 == 0) return half * half;
  return half * half * base;
}

}  // namespace math

namespace input {

template <typename T>
std::vector<T> load_vector(std::size_t size) {
  std::vector<T> result{};

  for (auto i = 0u; i < size; ++i) {
    T x;
    std::cin >> x;
    result.push_back(std::move(x));
  }

  return result;
}

}  // namespace input

namespace output {

template <typename T>
inline void answer(const T &ans) {
  cout << ans << "\n";
}

inline void yes() { cout << "YES\n"; }
inline void no() { cout << "NO\n"; }

inline void yesno(bool ans) {
  if (ans) {
    yes();
  } else {
    no();
  }
}

}  // namespace output

using namespace math;
using namespace input;
using namespace output;

#define LOOP(n) for (auto i = 0; i < n; ++i)

}  // namespace helpers

// for ‹N› test cases, uncomment for single test case
// #define SINGLE

namespace solution {

using namespace std;
using namespace helpers;

int find_ways(int k, const vector<int> &numbers) {
  std::array<int, 64> counters{};

  // try all possible ways
  for (int x : numbers) {
    std::array<int, 64> new_counters{};

    for (int mask = 0; mask < 64; ++mask) {
      new_counters[mask] = (new_counters[mask] + counters[mask]) % MODULO;
      new_counters[mask & x] =
          (new_counters[mask & x] + counters[mask]) % MODULO;
    }

    new_counters[x] = (new_counters[x] + 1) % MODULO;
    counters = move(new_counters);
  }

  // sum up the found results
  int counts = 0;
  for (auto i = 0u; i < 64; ++i) {
    if (popcount(i) == k) {
      counts = (counts + counters[i]) % MODULO;
    }
  }
  return counts;
}

void solve() {
  int n, k;
  cin >> n >> k;
  auto numbers = load_vector<int>(n);
  answer(find_ways(k, numbers));
}

}  // namespace solution

using namespace solution;

#ifdef TEST

#include "../.common/cpp/catch_amalgamated.hpp"

TEST_CASE("examples") {
  CHECK(find_ways(1, vector{1, 1, 1, 1, 1}) == 31);
  CHECK(find_ways(0, vector{0, 1, 2, 3}) == 10);
  CHECK(find_ways(1, vector{5, 5, 7, 4, 2}) == 10);
  CHECK(find_ways(2, vector{3}) == 1);
  CHECK(find_ways(0, vector{0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2}) == 4032);
  CHECK(find_ways(6, vector{63, 0, 63, 5, 5, 63, 63, 4, 12, 13}) == 15);
}

#else

int main(void) {

#ifdef SINGLE

  solution::solve();

#else

  // for multiple test cases
  int N;
  std::cin >> N >> std::ws;

  for (auto i = 0; i < N; ++i) {
    solution::solve();
  }

#endif

  return 0;
}

#endif
