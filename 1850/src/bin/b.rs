#![allow(unused_imports)]

use self::input::*;
use self::math::*;
use self::output::*;

fn find_idx(responses: &[Vec<i32>]) -> usize {
    let mut found_idx = responses.len();
    let mut found_quality = -1;

    for (i, r) in responses.iter().enumerate().filter(|(_, r)| r[0] <= 10) {
        if r[1] > found_quality {
            found_idx = i + 1;
            found_quality = r[1];
        }
    }

    found_idx
}

fn solve(s: &mut Scanner) {
    let n = s.next::<usize>();
    let input: Vec<Vec<i32>> = (0..n).map(|_| s.next_vec::<i32>(2)).collect();
    println!("{}", find_idx(&input));
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn examples() {
        assert_eq!(
            find_idx(&vec![
                vec![7, 2],
                vec![12, 5],
                vec![9, 3],
                vec![9, 4],
                vec![10, 1],
            ]),
            4
        );
        assert_eq!(find_idx(&vec![vec![1, 2], vec![3, 4], vec![5, 6],]), 3);
        assert_eq!(find_idx(&vec![vec![1, 43]]), 1);
    }
}

// region runner
const SINGLE_TEST: bool = false;
fn main() {
    let mut s = Scanner::new();

    if SINGLE_TEST {
        solve(&mut s)
    } else {
        let n = s.next::<usize>();
        for _ in 0..n {
            solve(&mut s)
        }
    }
}
// endregion runner

#[allow(dead_code)]
mod math {
    const MOD: i64 = 1_000_000_007;

    pub fn add(a: i64, b: i64) -> i64 {
        (a + b) % MOD
    }

    pub fn sub(a: i64, b: i64) -> i64 {
        ((a - b) % MOD + MOD) % MOD
    }

    pub fn mul(a: i64, b: i64) -> i64 {
        (a * b) % MOD
    }

    pub fn exp(b: i64, e: i64) -> i64 {
        if e == 0 {
            return 1;
        }

        let half = exp(b, e / 2);
        if e % 2 == 0 {
            return mul(half, half);
        }

        mul(half, mul(half, b))
    }
}

#[allow(dead_code)]
mod output {
    pub fn yes() {
        println!("YES");
    }

    pub fn no() {
        println!("NO");
    }

    pub fn yesno(ans: bool) {
        println!("{}", if ans { "YES" } else { "NO" });
    }
}

#[allow(dead_code)]
mod input {
    use std::collections::VecDeque;
    use std::io;
    use std::str::FromStr;

    pub struct Scanner {
        buffer: VecDeque<String>,
    }

    impl Scanner {
        pub fn new() -> Scanner {
            Scanner {
                buffer: VecDeque::new(),
            }
        }

        pub fn next<T: FromStr>(&mut self) -> T {
            if self.buffer.is_empty() {
                let mut input = String::new();

                io::stdin().read_line(&mut input).ok();

                for word in input.split_whitespace() {
                    self.buffer.push_back(word.to_string())
                }
            }

            let front = self.buffer.pop_front().unwrap();
            front.parse::<T>().ok().unwrap()
        }

        pub fn next_vec<T: FromStr>(&mut self, n: usize) -> Vec<T> {
            let mut arr = vec![];

            for _ in 0..n {
                arr.push(self.next::<T>());
            }

            arr
        }
    }
}
