#![allow(unused_imports)]

use self::input::*;
use self::math::*;
use self::output::*;

fn follow(grid: &[Vec<char>], mut y: usize, x: usize) -> String {
    let mut found: String = String::new();

    while y < grid.len() && grid[y][x] != '.' {
        found.push(grid[y][x]);
        y += 1;
    }

    found
}

fn find_word(grid: &[Vec<char>]) -> String {
    for (y, x) in (0..8).flat_map(|y| (0..8).map(move |x| (y, x))) {
        if grid[y][x] != '.' {
            return follow(grid, y, x);
        }
    }

    unreachable!()
}

fn solve(s: &mut Scanner) {
    let lines: Vec<Vec<char>> = s
        .next_vec::<String>(8)
        .into_iter()
        .map(|l| l.chars().collect())
        .collect();

    println!("{}", find_word(&lines));
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn examples() {
        assert_eq!(
            find_word(
                &vec![
                    "........", "........", "........", "........", "...i....", "........",
                    "........", "........",
                ]
                .iter()
                .map(|s| s.chars().collect::<Vec<char>>())
                .collect::<Vec<Vec<char>>>()
            ),
            "i".to_owned()
        );
        assert_eq!(
            find_word(
                &vec![
                    "........", ".l......", ".o......", ".s......", ".t......", "........",
                    "........", "........",
                ]
                .iter()
                .map(|s| s.chars().collect::<Vec<char>>())
                .collect::<Vec<Vec<char>>>()
            ),
            "lost".to_owned()
        );
        assert_eq!(
            find_word(
                &vec![
                    "........", "........", "........", "........", "......t.", "......h.",
                    "......e.", "........",
                ]
                .iter()
                .map(|s| s.chars().collect::<Vec<char>>())
                .collect::<Vec<Vec<char>>>()
            ),
            "the".to_owned()
        );
        assert_eq!(
            find_word(
                &vec![
                    "........", "........", "........", "........", ".......g", ".......a",
                    ".......m", ".......e",
                ]
                .iter()
                .map(|s| s.chars().collect::<Vec<char>>())
                .collect::<Vec<Vec<char>>>()
            ),
            "game".to_owned()
        );
        assert_eq!(
            find_word(
                &vec![
                    "a.......", "a.......", "a.......", "a.......", "a.......", "a.......",
                    "a.......", "a.......",
                ]
                .iter()
                .map(|s| s.chars().collect::<Vec<char>>())
                .collect::<Vec<Vec<char>>>()
            ),
            "aaaaaaaa".to_owned()
        );
    }
}

// region runner
const SINGLE_TEST: bool = false;
fn main() {
    let mut s = Scanner::new();

    if SINGLE_TEST {
        solve(&mut s)
    } else {
        let n = s.next::<usize>();
        for _ in 0..n {
            solve(&mut s)
        }
    }
}
// endregion runner

#[allow(dead_code)]
mod math {
    const MOD: i64 = 1_000_000_007;

    pub fn add(a: i64, b: i64) -> i64 {
        (a + b) % MOD
    }

    pub fn sub(a: i64, b: i64) -> i64 {
        ((a - b) % MOD + MOD) % MOD
    }

    pub fn mul(a: i64, b: i64) -> i64 {
        (a * b) % MOD
    }

    pub fn exp(b: i64, e: i64) -> i64 {
        if e == 0 {
            return 1;
        }

        let half = exp(b, e / 2);
        if e % 2 == 0 {
            return mul(half, half);
        }

        mul(half, mul(half, b))
    }
}

#[allow(dead_code)]
mod output {
    pub fn yes() {
        println!("YES");
    }

    pub fn no() {
        println!("NO");
    }

    pub fn yesno(ans: bool) {
        println!("{}", if ans { "YES" } else { "NO" });
    }
}

#[allow(dead_code)]
mod input {
    use std::collections::VecDeque;
    use std::io;
    use std::str::FromStr;

    pub struct Scanner {
        buffer: VecDeque<String>,
    }

    impl Scanner {
        pub fn new() -> Scanner {
            Scanner {
                buffer: VecDeque::new(),
            }
        }

        pub fn next<T: FromStr>(&mut self) -> T {
            if self.buffer.is_empty() {
                let mut input = String::new();

                io::stdin().read_line(&mut input).ok();

                for word in input.split_whitespace() {
                    self.buffer.push_back(word.to_string())
                }
            }

            let front = self.buffer.pop_front().unwrap();
            front.parse::<T>().ok().unwrap()
        }

        pub fn next_vec<T: FromStr>(&mut self, n: usize) -> Vec<T> {
            let mut arr = vec![];

            for _ in 0..n {
                arr.push(self.next::<T>());
            }

            arr
        }
    }
}
