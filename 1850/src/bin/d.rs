#![allow(unused_imports)]

// region ‹use›
use self::input::*;
use self::math::*;
use self::output::*;
use std::cmp::{max, min};
// endregion ‹use›

fn min_to_satisfy(k: i32, mut diffs: Vec<i32>) -> i32 {
    diffs.sort();

    let mut longest = 0;
    let mut current_longest = 1;
    for (&x, &y) in diffs.iter().zip(diffs.iter().skip(1)) {
        if y - x > k {
            longest = max(longest, current_longest);
            current_longest = 1;
        } else {
            current_longest += 1;
        }
    }
    longest = max(longest, current_longest);

    diffs.len() as i32 - longest
}

fn solve(s: &mut Scanner) {
    let n = s.next::<usize>();
    let k = s.next::<i32>();
    let diffs = s.next_vec::<i32>(n);

    println!("{}", min_to_satisfy(k, diffs));
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn examples() {
        assert_eq!(min_to_satisfy(1, vec![1, 2, 4, 5, 6]), 2);
        assert_eq!(min_to_satisfy(2, vec![10]), 0);
        assert_eq!(min_to_satisfy(3, vec![17, 3, 1, 20, 12, 5, 17, 12]), 5);
        assert_eq!(min_to_satisfy(2, vec![2, 4, 6, 8]), 0);
        assert_eq!(min_to_satisfy(3, vec![2, 3, 19, 10, 8]), 3);
        assert_eq!(min_to_satisfy(4, vec![1, 10, 5]), 1);
        assert_eq!(min_to_satisfy(1, vec![8, 3, 1, 4, 5, 10, 7, 3]), 4);
    }
}

// region runner
const SINGLE_TEST: bool = false;
fn main() {
    let mut s = Scanner::new();

    if SINGLE_TEST {
        solve(&mut s)
    } else {
        let n = s.next::<usize>();
        for _ in 0..n {
            solve(&mut s)
        }
    }
}
// endregion runner

#[allow(dead_code)]
mod math {
    const MOD: i64 = 1_000_000_007;

    pub fn add(a: i64, b: i64) -> i64 {
        (a + b) % MOD
    }

    pub fn sub(a: i64, b: i64) -> i64 {
        ((a - b) % MOD + MOD) % MOD
    }

    pub fn mul(a: i64, b: i64) -> i64 {
        (a * b) % MOD
    }

    pub fn exp(b: i64, e: i64) -> i64 {
        if e == 0 {
            return 1;
        }

        let half = exp(b, e / 2);
        if e % 2 == 0 {
            return mul(half, half);
        }

        mul(half, mul(half, b))
    }
}

#[allow(dead_code)]
mod output {
    pub fn yes() {
        println!("YES");
    }

    pub fn no() {
        println!("NO");
    }

    pub fn yesno(ans: bool) {
        println!("{}", if ans { "YES" } else { "NO" });
    }
}

#[allow(dead_code)]
mod input {
    use std::collections::VecDeque;
    use std::io;
    use std::str::FromStr;

    pub struct Scanner {
        buffer: VecDeque<String>,
    }

    impl Scanner {
        pub fn new() -> Scanner {
            Scanner {
                buffer: VecDeque::new(),
            }
        }

        pub fn next<T: FromStr>(&mut self) -> T {
            if self.buffer.is_empty() {
                let mut input = String::new();

                io::stdin().read_line(&mut input).ok();

                for word in input.split_whitespace() {
                    self.buffer.push_back(word.to_string())
                }
            }

            let front = self.buffer.pop_front().unwrap();
            front.parse::<T>().ok().unwrap()
        }

        pub fn next_vec<T: FromStr>(&mut self, n: usize) -> Vec<T> {
            let mut arr = vec![];

            for _ in 0..n {
                arr.push(self.next::<T>());
            }

            arr
        }
    }
}
