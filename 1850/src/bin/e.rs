#![allow(unused_imports)]

// region ‹use›
use self::input::*;
use self::math::*;
use self::output::*;
use std::cmp::{max, min};
// endregion ‹use›

fn find_w(covered: i64, sizes: Vec<i64>) -> i64 {
    let s: i64 = sizes.iter().sum();
    let s_squared: i64 = sizes.iter().map(|s| s * s).sum();

    let a = 4 * sizes.len() as i128;
    let b = (4 * s) as i128;
    let c = (s_squared - covered) as i128;
    let d = b * b - 4 * a * c;

    ((-b + d.isqrt()) / (2 * a)).try_into().unwrap()
}

fn solve(s: &mut Scanner) {
    let n = s.next::<usize>();
    let c = s.next::<i64>();
    let sizes = s.next_vec::<i64>(n);
    println!("{}", find_w(c, sizes));
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn examples() {
        assert_eq!(find_w(50, vec![3, 2, 1]), 1);
        assert_eq!(find_w(100, vec![6]), 2);
        assert_eq!(find_w(500, vec![2, 2, 2, 2, 2]), 4);
        assert_eq!(find_w(365, vec![3, 4]), 5);
        assert_eq!(find_w(469077255466389, vec![10000, 2023]), 7654321);
        assert_eq!(
            find_w(
                635472106413848880,
                vec![9181, 4243, 7777, 1859, 2017, 4397, 14, 9390, 2245, 7225]
            ),
            126040443
        );
        assert_eq!(
            find_w(
                176345687772781240,
                vec![9202, 9407, 9229, 6257, 7743, 5738, 7966]
            ),
            79356352
        );
        assert_eq!(
            find_w(
                865563946464579627,
                vec![
                    3654, 5483, 1657, 7571, 1639, 9815, 122, 9468, 3079, 2666, 5498, 4540, 7861,
                    5384
                ]
            ),
            124321725
        );
        assert_eq!(
            find_w(
                977162053008871403,
                vec![
                    9169, 9520, 9209, 9013, 9300, 9843, 9933, 9454, 9960, 9167, 9964, 9701, 9251,
                    9404, 9462, 9277, 9661, 9164, 9161
                ]
            ),
            113385729
        );
        assert_eq!(
            find_w(
                886531871815571953,
                vec![
                    2609, 10, 5098, 9591, 949, 8485, 6385, 4586, 1064, 5412, 6564, 8460, 2245,
                    6552, 5089, 8353, 3803, 3764
                ]
            ),
            110961227
        );
    }
}

// region runner
const SINGLE_TEST: bool = false;
fn main() {
    let mut s = Scanner::new();

    if SINGLE_TEST {
        solve(&mut s)
    } else {
        let n = s.next::<usize>();
        for _ in 0..n {
            solve(&mut s)
        }
    }
}
// endregion runner

#[allow(dead_code)]
mod math {
    const MOD: i64 = 1_000_000_007;

    pub fn add(a: i64, b: i64) -> i64 {
        (a + b) % MOD
    }

    pub fn sub(a: i64, b: i64) -> i64 {
        ((a - b) % MOD + MOD) % MOD
    }

    pub fn mul(a: i64, b: i64) -> i64 {
        (a * b) % MOD
    }

    pub fn exp(b: i64, e: i64) -> i64 {
        if e == 0 {
            return 1;
        }

        let half = exp(b, e / 2);
        if e % 2 == 0 {
            return mul(half, half);
        }

        mul(half, mul(half, b))
    }

    /// A trait implementing the unsigned bit shifts.
    pub trait UnsignedShift {
        fn unsigned_shl(self, n: u32) -> Self;
        fn unsigned_shr(self, n: u32) -> Self;
    }

    /// A trait implementing the integer square root.
    pub trait ISqrt {
        /// Find the integer square root.
        ///
        /// See [Integer_square_root on wikipedia][wiki_article] for more information (and also the
        /// source of this algorithm)
        ///
        /// # Panics
        ///
        /// For negative numbers (`i` family) this function will panic on negative input
        ///
        /// [wiki_article]: https://en.wikipedia.org/wiki/Integer_square_root
        fn isqrt(&self) -> Self
        where
            Self: Sized,
        {
            self.isqrt_checked()
                .expect("cannot calculate square root of negative number")
        }

        /// Find the integer square root, returning `None` if the number is negative (this can never
        /// happen for unsigned types).
        fn isqrt_checked(&self) -> Option<Self>
        where
            Self: Sized;
    }

    macro_rules! math_traits_impl {
        ($T:ty, $U: ty) => {
            impl UnsignedShift for $T {
                #[inline]
                fn unsigned_shl(self, n: u32) -> Self {
                    ((self as $U) << n) as $T
                }

                #[inline]
                fn unsigned_shr(self, n: u32) -> Self {
                    ((self as $U) >> n) as $T
                }
            }

            impl ISqrt for $T {
                #[inline]
                fn isqrt_checked(&self) -> Option<Self> {
                    use core::cmp::Ordering;
                    match self.cmp(&<$T>::default()) {
                        // Hopefully this will be stripped for unsigned numbers (impossible condition)
                        Ordering::Less => return None,
                        Ordering::Equal => return Some(<$T>::default()),
                        _ => {}
                    }

                    // Compute bit, the largest power of 4 <= n
                    let max_shift: u32 = <$T>::default().leading_zeros() - 1;
                    let shift: u32 = (max_shift - self.leading_zeros()) & !1;
                    let mut bit = <$T>::try_from(1).unwrap().unsigned_shl(shift);

                    // Algorithm based on the implementation in:
                    // https://en.wikipedia.org/wiki/Methods_of_computing_square_roots#Binary_numeral_system_(base_2)
                    // Note that result/bit are logically unsigned (even if T is signed).
                    let mut n = *self;
                    let mut result = <$T>::default();
                    while bit != <$T>::default() {
                        if n >= (result + bit) {
                            n -= result + bit;
                            result = result.unsigned_shr(1) + bit;
                        } else {
                            result = result.unsigned_shr(1);
                        }
                        bit = bit.unsigned_shr(2);
                    }
                    Some(result)
                }
            }
        };
    }

    math_traits_impl!(i8, u8);
    math_traits_impl!(u8, u8);
    math_traits_impl!(i16, u16);
    math_traits_impl!(u16, u16);
    math_traits_impl!(i32, u32);
    math_traits_impl!(u32, u32);
    math_traits_impl!(i64, u64);
    math_traits_impl!(u64, u64);
    math_traits_impl!(i128, u128);
    math_traits_impl!(u128, u128);
    math_traits_impl!(isize, usize);
    math_traits_impl!(usize, usize);
}

#[allow(dead_code)]
mod output {
    pub fn yes() {
        println!("YES");
    }

    pub fn no() {
        println!("NO");
    }

    pub fn yesno(ans: bool) {
        println!("{}", if ans { "YES" } else { "NO" });
    }
}

#[allow(dead_code)]
mod input {
    use std::collections::VecDeque;
    use std::io;
    use std::str::FromStr;

    pub struct Scanner {
        buffer: VecDeque<String>,
    }

    impl Scanner {
        pub fn new() -> Scanner {
            Scanner {
                buffer: VecDeque::new(),
            }
        }

        pub fn next<T: FromStr>(&mut self) -> T {
            if self.buffer.is_empty() {
                let mut input = String::new();

                io::stdin().read_line(&mut input).ok();

                for word in input.split_whitespace() {
                    self.buffer.push_back(word.to_string())
                }
            }

            let front = self.buffer.pop_front().unwrap();
            front.parse::<T>().ok().unwrap()
        }

        pub fn next_vec<T: FromStr>(&mut self, n: usize) -> Vec<T> {
            let mut arr = vec![];

            for _ in 0..n {
                arr.push(self.next::<T>());
            }

            arr
        }
    }
}
