#include <iostream>
#include <string>

namespace {

using namespace std;

int remove(const std::string& s) {
  int counter = 0;

  for (auto i = 0; i < s.size() - 1; ++i) {
    if (s[i] == s[i + 1]) {
      ++counter;
    }
  }

  return counter;
}

void solve() {
  int size;
  cin >> size;

  std::string line;
  cin >> line;

  cout << remove(line) << "\n";
}

}  // namespace

// for single test case, comment out for ‹N› test cases
#define SINGLE

#ifndef TEST

int main(void) {
#ifdef SINGLE

  solve();

#else

  // for multiple test cases
  int N;
  std::cin >> N >> std::ws;

  for (auto i = 0; i < N; ++i) {
    solve();
  }

#endif

  return 0;
}

#else

#include "../.common/cpp/catch_amalgamated.hpp"

TEST_CASE("examples") {
  CHECK(remove(std::string("RRG")) == 1);
  CHECK(remove(std::string("RRRRR")) == 4);
  CHECK(remove(std::string("BRBG")) == 0);
}

#endif