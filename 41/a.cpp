#include <algorithm>
#include <cassert>
#include <cctype>
#include <cstdint>
#include <functional>
#include <iostream>
#include <optional>
#include <sstream>
#include <string>
#include <vector>

namespace helpers {

using namespace std;

long pow(long base, long exp) {
  if (exp == 0) return 1;
  long half = pow(base, exp / 2);
  if (exp % 2 == 0) return half * half;
  return half * half * base;
}

template <typename T>
inline void answer(const T& ans) {
  cout << ans << "\n";
}

inline void yes() { cout << "YES\n"; }
inline void no() { cout << "NO\n"; }

inline void yesno(bool answer) {
  if (answer) {
    yes();
  } else {
    no();
  }
}

}  // namespace helpers

namespace {

using namespace std;
using namespace helpers;

bool berlandish(const std::string& a, std::string b) {
  std::reverse(b.begin(), b.end());
  return a == b;
}

void solve() {
  std::string l, r;
  cin >> l >> r;

  yesno(berlandish(l, r));
}

}  // namespace

// for single test case, comment out for ‹N› test cases
#define SINGLE

#ifdef TEST

#include "../.common/cpp/catch_amalgamated.hpp"

TEST_CASE("examples") {
  CHECK(berlandish("code", "edoc"));
  CHECK(berlandish("edoc", "code"));

  CHECK(!berlandish("abb", "aba"));
  CHECK(!berlandish("aba", "abb"));

  CHECK(!berlandish("code", "code"));
}

#else

int main(void) {

#ifdef SINGLE

  solve();

#else

  // for multiple test cases
  int N;
  std::cin >> N >> std::ws;

  for (auto i = 0; i < N; ++i) {
    solve();
  }

#endif

  return 0;
}

#endif
