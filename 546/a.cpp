#include <iostream>

namespace {

using namespace std;

long pow(long base, long exp) {
  if (exp == 0) return 1;
  long half = pow(base, exp / 2);
  if (exp % 2 == 0) return half * half;
  return half * half * base;
}

int borrows(int k, int n, int w) {
  int total = k * (1 + w) * w / 2;

  return max(total - n, 0);
}

void solve() {
  int k, n, w;
  cin >> k >> n >> w;

  cout << borrows(k, n, w) << "\n";
}

}  // namespace

// for single test case, comment out for ‹N› test cases
#define SINGLE

#ifndef TEST

int main(void) {
#ifdef SINGLE

  solve();

#else

  // for multiple test cases
  int N;
  std::cin >> N >> std::ws;

  for (auto i = 0; i < N; ++i) {
    solve();
  }

#endif

  return 0;
}

#else

#include "../.common/cpp/catch_amalgamated.hpp"

TEST_CASE("examples") {
  CHECK(borrows(3, 17, 4) == 13);
  CHECK(borrows(3, 100, 4) == 0);
}

#endif