#include <algorithm>
#include <cctype>
#include <functional>
#include <iostream>
#include <string>
#include <vector>

namespace helpers {

using namespace std;

long pow(long base, long exp) {
  if (exp == 0) return 1;
  long half = pow(base, exp / 2);
  if (exp % 2 == 0) return half * half;
  return half * half * base;
}

template <typename T>
void answer(const T &ans) {
  cout << ans << "\n";
}

}  // namespace helpers

namespace {

using namespace std;
using namespace helpers;

string correct(string w) {
  auto upper = count_if(w.begin(), w.end(), [](auto &c) { return isupper(c); });

  if (upper > w.size() - upper) {
    transform(w.begin(), w.end(), w.begin(),
              [&](char c) { return toupper(c); });
  } else {
    transform(w.begin(), w.end(), w.begin(),
              [&](char c) { return tolower(c); });
  }

  return w;
}

void solve() {
  std::string w;
  cin >> w;

  answer(correct(w));
}

}  // namespace

// for single test case, comment out for ‹N› test cases
#define SINGLE

#ifdef TEST

#include "../.common/cpp/catch_amalgamated.hpp"

TEST_CASE("examples") {
  CHECK(correct("HoUse") == "house");
  CHECK(correct("ViP") == "VIP");
  CHECK(correct("maTRIx") == "matrix");
}

#else

int main(void) {

#ifdef SINGLE

  solve();

#else

  // for multiple test cases
  int N;
  std::cin >> N >> std::ws;

  for (auto i = 0; i < N; ++i) {
    solve();
  }

#endif

  return 0;
}

#endif
