#include <iostream>

namespace helpers {

using namespace std;

long pow(long base, long exp) {
  if (exp == 0) return 1;
  long half = pow(base, exp / 2);
  if (exp % 2 == 0) return half * half;
  return half * half * base;
}

template <typename T>
void answer(const T &ans) {
  cout << ans << "\n";
}

}  // namespace helpers

namespace {

using namespace std;
using namespace helpers;

int steps(int distance) {
  int count = 0;

  for (auto step : {5, 4, 3, 2, 1}) {
    count += distance / step;
    distance %= step;
  }

  return count;
}

void solve() {
  int d;
  cin >> d;
  answer(steps(d));
}

}  // namespace

// for single test case, comment out for ‹N› test cases
#define SINGLE

#ifdef TEST

#include "../.common/cpp/catch_amalgamated.hpp"

TEST_CASE("examples") {
  CHECK(steps(5) == 1);
  CHECK(steps(12) == 3);
}

#else

int main(void) {

#ifdef SINGLE

  solve();

#else

  // for multiple test cases
  int N;
  std::cin >> N >> std::ws;

  for (auto i = 0; i < N; ++i) {
    solve();
  }

#endif

  return 0;
}

#endif
