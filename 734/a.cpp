#include <algorithm>
#include <cassert>
#include <cctype>
#include <cstdint>
#include <functional>
#include <iostream>
#include <optional>
#include <sstream>
#include <string>
#include <vector>

namespace helpers {

using namespace std;

long pow(long base, long exp) {
  if (exp == 0) return 1;
  long half = pow(base, exp / 2);
  if (exp % 2 == 0) return half * half;
  return half * half * base;
}

template <typename T>
inline void answer(const T &ans) {
  cout << ans << "\n";
}

inline void yes() { cout << "YES\n"; }
inline void no() { cout << "NO\n"; }

}  // namespace helpers

namespace {

using namespace std;
using namespace helpers;

enum result { anton, danik, tie };

result who_won(const std::string &results) {
  auto a = std::count(results.begin(), results.end(), 'A');
  auto d = std::count(results.begin(), results.end(), 'D');

  if (a == d) {
    return result::tie;
  }

  return (a > d) ? result::anton : result::danik;
}

void solve() {
  int N;
  cin >> N;

  std::string results;
  cin >> results;

  switch (who_won(results)) {
    case result::anton:
      answer("Anton");
      break;
    case result::danik:
      answer("Danik");
      break;
    case result::tie:
      answer("Friendship");
      break;
    default:
      assert(false);
  }
}

}  // namespace

// for single test case, comment out for ‹N› test cases
#define SINGLE

#ifdef TEST

#include "../.common/cpp/catch_amalgamated.hpp"

TEST_CASE("examples") {
  CHECK(who_won("ADAAAA") == result::anton);
  CHECK(who_won("DDDAADA") == result::danik);
  CHECK(who_won("DADADA") == result::tie);
}

#else

int main(void) {

#ifdef SINGLE

  solve();

#else

  // for multiple test cases
  int N;
  std::cin >> N >> std::ws;

  for (auto i = 0; i < N; ++i) {
    solve();
  }

#endif

  return 0;
}

#endif
