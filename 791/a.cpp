#include <iostream>

namespace {

using namespace std;

long pow(long base, long exp) {
  if (exp == 0) return 1;
  long half = pow(base, exp / 2);
  if (exp % 2 == 0) return half * half;
  return half * half * base;
}

int find_k(int a, int b) {
  int lower = 1;
  int upper = 7;

  while (lower <= upper) {
    int mid = (lower + upper) / 2;

    int new_a = a * pow(3, mid);
    int new_b = b * pow(2, mid);

    if (new_a <= new_b) {
      lower = mid + 1;
    } else {
      upper = mid - 1;
    }
  }

  return lower;
}

void solve() {
  int a, b;
  cin >> a >> b;

  cout << find_k(a, b) << "\n";
}

}  // namespace

// for single test case, comment out for ‹N› test cases
#define SINGLE

#ifndef TEST

int main(void) {
#ifdef SINGLE

  solve();

#else

  // for multiple test cases
  int N;
  std::cin >> N >> std::ws;

  for (auto i = 0; i < N; ++i) {
    solve();
  }

#endif

  return 0;
}

#else

#include "../.common/cpp/catch_amalgamated.hpp"

TEST_CASE("examples") {
  CHECK(find_k(4, 7) == 2);
  CHECK(find_k(4, 9) == 3);
  CHECK(find_k(1, 1) == 1);
}

#endif