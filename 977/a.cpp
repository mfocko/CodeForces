#include <iostream>

namespace helpers {

using namespace std;

long pow(long base, long exp) {
  if (exp == 0) return 1;
  long half = pow(base, exp / 2);
  if (exp % 2 == 0) return half * half;
  return half * half * base;
}

template <typename T>
inline void answer(const T &ans) {
  cout << ans << "\n";
}

}  // namespace helpers

namespace {

using namespace std;
using namespace helpers;

int decrease(int n, int k) {
  while (k > 0) {
    if (n % 10 != 0) {
      auto steps = min(n % 10, k);

      n -= steps;
      k -= steps;
    } else {
      n /= 10;
      --k;
    }
  }

  return n;
}

void solve() {
  int n, k;
  cin >> n >> k;

  answer(decrease(n, k));
}

}  // namespace

// for single test case, comment out for ‹N› test cases
#define SINGLE

#ifdef TEST

#include "../.common/cpp/catch_amalgamated.hpp"

TEST_CASE("examples") {
  CHECK(decrease(512, 4) == 50);
  CHECK(decrease(1000000000, 9) == 1);
}

#else

int main(void) {

#ifdef SINGLE

  solve();

#else

  // for multiple test cases
  int N;
  std::cin >> N >> std::ws;

  for (auto i = 0; i < N; ++i) {
    solve();
  }

#endif

  return 0;
}

#endif
