#!/bin/sh

function get_contest() {
    mkdir $1
    curl -Lk https://codeforces.com/contest/$1/problems -o $1/index.html
}

for contest in "$@"
do
    get_contest $contest;
done
